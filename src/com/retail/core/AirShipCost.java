package com.retail.core;

public class AirShipCost implements ShippingCostCal {
	

	public Double calShipCost(Item item)
	{
		String u = item.getUpc().substring(10, 11);	
		int d  = Integer.parseInt(u);
		Double shipCost = item.getWeight() * d;
		return (Math.ceil(shipCost*100) / 100);
	}

}
