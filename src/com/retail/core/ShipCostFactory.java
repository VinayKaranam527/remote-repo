package com.retail.core;

public class ShipCostFactory {
	public ShippingCostCal getShipCost(String shipType){
	      if(shipType == null){
	         return null;
	      }		
	      if(shipType.equalsIgnoreCase("AIR")){
	         return new AirShipCost();
	         
	      } else if(shipType.equalsIgnoreCase("GROUND")){
	         return new GroundShipCost();
	         
	      }
	      
	      return null;
	   }


}
