package com.retail.core;

public class Item {
	private String upc;
	private String description;
	private float price;
	private Double weight;
	private String shippingType;

	
	

	public Item(String upc,String description,float price,Double weight,String shippingType)
	{
		this.upc = upc;
		this.description = description;
		this.price=price;
		this.weight= weight;
		this.shippingType = shippingType;
	}

	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	public String getShippingType() {
		return shippingType;
	}
	public void setShippingType(String shippingType) {
		this.shippingType = shippingType;
	}	
	

}
