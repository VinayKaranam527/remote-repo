package com.retail.core;

public class GroundShipCost implements ShippingCostCal {

	public Double calShipCost(Item item)
	{
		
		Double shipCost = (item.getWeight() * 2.5);
		//Double sCost = shipCost.doubleValue();
		return (Math.ceil(shipCost*100) / 100);
	}

}
